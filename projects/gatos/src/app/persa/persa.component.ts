import { Component, OnInit } from '@angular/core';
import { PersaService } from './persa.service';

@Component({
  selector: 'gatos-persa',
  templateUrl: './persa.component.html',
  styleUrls: ['./persa.component.css']
})

export class PersaComponent implements OnInit {
  saludo: string;

  constructor(private persaService: PersaService) { }

  ngOnInit() {
    this.saludo = this.persaService.saludar()
  }


}
