import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersaService {

  constructor() { }

  saludar() {
    return `Hola, soy un gato persa.`;
  }
}
