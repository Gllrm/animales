import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersaRoutingModule } from './persa-routing.module';
import { PersaComponent } from './persa.component';

@NgModule({
  imports: [
    CommonModule,
    PersaRoutingModule
  ],
  declarations: [PersaComponent]
})
export class PersaModule { }
