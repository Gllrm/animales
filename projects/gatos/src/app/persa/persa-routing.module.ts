import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersaComponent } from './persa.component';

const routes: Routes = [
  { path: 'persa', component: PersaComponent },
  { path: '', redirectTo: 'persa', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersaRoutingModule { }
