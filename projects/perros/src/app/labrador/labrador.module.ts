import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LabradorRoutingModule } from './labrador-routing.module';
import { LabradorComponent } from './labrador.component';

@NgModule({
  imports: [
    CommonModule,
    LabradorRoutingModule
  ],
  declarations: [LabradorComponent]
})
export class LabradorModule { }
