import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LabradorComponent } from './labrador.component';

const routes: Routes = [
  { path: 'labrador', component: LabradorComponent},
  { path: '', redirectTo: 'labrador', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LabradorRoutingModule { }
