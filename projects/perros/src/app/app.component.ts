import { Component } from '@angular/core';

@Component({
  selector: 'perros-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'perros';
}
