import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AguilaRoutingModule } from './aguila-routing.module';
import { AguilaComponent } from './aguila.component';

@NgModule({
  imports: [
    CommonModule,
    AguilaRoutingModule
  ],
  declarations: [AguilaComponent]
})
export class AguilaModule { }
