import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AguilaComponent } from './aguila.component';

const routes: Routes = [
  { path: '', component: AguilaComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AguilaRoutingModule { }
