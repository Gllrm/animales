import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [

  { path: 'aguilas', loadChildren: './aguila/aguila.module#AguilaModule'},
  { path: 'perros', loadChildren: 'projects/perros/src/app/labrador/labrador.module#LabradorModule'},
  { path: 'gatos', loadChildren: 'projects/gatos/src/app/persa/persa.module#PersaModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
